class MessagesController < ApplicationController
  protect_from_forgery :except => :create 

  def index
    @messages = Message.where({:active => true}).order("created_at ASC")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @messages }
    end
  end

  def show
    @message = Message.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @message }
    end
  end

  def create
    @message = Message.new(message_params)

    respond_to do |format|
      if @message.save
        msg = { :status => "OK", :message => "Success!" }
        format.json { render :json => msg}
      else
        msg = { :status => "FAIL", :message => "Whoops, something went terribly wrong!" }
        format.json { render :json => msg}
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(:app_id, :content, :username)
  end
end
