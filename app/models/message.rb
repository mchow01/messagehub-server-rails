class Message < ActiveRecord::Base
	#attr_accessible :app_id, :content, :username
	belongs_to :app
 
	validates_presence_of :app_id, :content, :username
	validates_format_of :username, :with => /^[A-Za-z0-9_\-.&]*\z/, :multiline => true
	validates :username, :length => { :minimum => 3,
		:maximum => 60,
		:too_long => "%{count} characters is the maximum allowed" }
	#validates :content, :length => { :maximum => 160,
	#	:too_long => "%{count} characters is the maximum allowed" }
end
