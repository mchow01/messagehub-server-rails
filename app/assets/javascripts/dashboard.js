//= require jquery
//= require jquery_ujs
//= require_tree .

var last = 0;

function htmlEncode(value) {
	//return $('<div/>').text(value).html();
	return value;
}

function updateMessages() {
	$.getJSON('/messages.json', function(data) {
		if (data.length == 0 && last == 0) {
			$(".messages").html("<h4>Uh, no content to display.  Wait for a while...</h4>").hide().fadeIn("slow");
		}
		else if (data.length > last) {
			if (last == 0) {
				$(".messages").html("");
			}
			for (i = last; i < data.length; i++) {
			    $(".messages").prepend('<div class="message"><p class="content">' + htmlEncode(data[i]["content"]) + '</p><p class="username">' + htmlEncode(data[i]["username"]) + ', <a href="messages/' + data[i]["id"] + '">' + jQuery.timeago(data[i]["created_at"]) + '</a></p></div>').hide().fadeIn("slow");
				last++;
			}
		}
	});
	
	// http://stackoverflow.com/questions/6685396/execute-the-first-time-the-setinterval-without-delay
	setTimeout(updateMessages, 30000);
}

$(document).ready(function() {
	updateMessages();
});
