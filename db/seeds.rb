# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# WTF? http://stackoverflow.com/questions/8136875/ruby-1-9-2-syntax-error

App.create(app_title: 'Unknown', description: 'Unknown')
App.create(app_title: 'Web', description: 'Via web')
App.create(app_title: 'iOS', description: 'Via iOS')
App.create(app_title: 'Android', description: 'Via Android')
App.create(app_title: 'Postman', description: 'Via Postman')
