class AddDefaultValueToActiveAttribute < ActiveRecord::Migration
  def change
  	change_column :apps, :active, :boolean, :default => true
  	change_column :messages, :active, :boolean, :default => true
  end
end
